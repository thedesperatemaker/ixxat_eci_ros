#include <EciApi130.hpp>
#include "EciCommon.h"

#include "asio.hpp"
//////////////////////////////////////////////////////////////////////////
// static constants, types, macros, variables

/** Look up table to convert the CAN FD DLC code into the real CAN data length */
static const BYTE ECI_CAN_FD_DLC_LUT[16] = { 0,1,2,3,4,5,6,7,8,12,16,20,24,32,48,64 };

// namespace eci_api{ 

    EciApi130::EciApi130(std::string can_port): AsyncPort(can_port),
        socketcan_stream_(EciApi130::io_context_) {}
    
    void EciApi130::InitEciDevice(){
        ECI_HW_PARA astcHwPara[4] = {{0}};
        ECI_HW_INFO stcHwInfo     = {0};

        OS_Printf("\n>> ECI Api for CAN@net NT <<\n");
        
        //*** Prepare Hardware parameter structure for multiple boards
        for(int dwIndex=0; dwIndex < _countof(astcHwPara); dwIndex++)
        {
            astcHwPara[dwIndex].wHardwareClass = ECI_HW_IP;
            strncpy(astcHwPara[dwIndex].u.sIpSettings.u.V1.szIpAddress, DEV_ADDR,
                    sizeof(astcHwPara[dwIndex].u.sIpSettings.u.V1.szIpAddress));
            astcHwPara[dwIndex].u.sIpSettings.u.V1.adwPort[0] = DEV_PORT;
            #ifdef ECIDEMO_HWUSEPOLLINGMODE
            astcHwPara[dwIndex].dwFlags = ECI_SETTINGS_FLAG_POLLING_MODE;
            #endif //ECIDEMO_HWUSEPOLLINGMODE
        }
        OS_Printf("\n>> Using CAN@net NT device %s:%d <<\n",
                    astcHwPara[0].u.sIpSettings.u.V1.szIpAddress,
                    astcHwPara[0].u.sIpSettings.u.V1.adwPort[0]);
        //*** At first call Initialize to prepare ECI driver
        hResult = ECI130_Initialize(_countof(astcHwPara), astcHwPara);
        ECIDEMO_CHECKERROR(ECI130_Initialize); //TODO UNCOMMENT

        //*** Retrieve hardware info
        if(ECI_OK == hResult)
        {
            //*** Retrieve hardware info
            hResult = ECI130_GetInfo(dwHwIndex, &stcHwInfo);
            ECIDEMO_CHECKERROR(ECI130_GetInfo);
            if(ECI_OK == hResult)
                EciPrintHwInfo(&stcHwInfo);
        }

        //*** Find first CAN Controller of Board
        if(ECI_OK == hResult)
        {
            hResult = EciGetNthCtrlOfClass(&stcHwInfo,
                                        ECI_CTRL_CAN,
                                        0, //first relative controller
                                        &dwCtrlIndex);
        }
    }
    bool EciApi130::SetupPort() {
        

        if(ECI_OK == hResult)
        {
            //*** Use Basic settings to open controller
            stcCtrlConfig.wCtrlClass                 = ECI_CTRL_CAN;
            stcCtrlConfig.u.sCanConfig.dwVer         = ECI_STRUCT_VERSION_V0;
            stcCtrlConfig.u.sCanConfig.u.V0.bBtReg0  = ECI_CAN_BT0_500KB;
            stcCtrlConfig.u.sCanConfig.u.V0.bBtReg1  = ECI_CAN_BT1_500KB;
            stcCtrlConfig.u.sCanConfig.u.V0.bOpMode  = ECI_CAN_OPMODE_STANDARD;

            //*** Open and Initialize given Controller of given board
            hResult = ECI130_CtrlOpen(&dwCtrlHandle, dwHwIndex, dwCtrlIndex, &stcCtrlConfig);
            // ECIDEMO_CHECKERROR(ECI130_CtrlOpen);
        }

        #if ASIO_VERSION < 101200L
        io_context_.post(std::bind(&EciApi130::ReadFromPort, this,
                                    std::ref(rcv_frame_)));

        #else
        asio::post(io_context_,
                    std::bind(&EciApi130::ReadFromPort, this, std::ref(rcv_frame_)));
        #endif

        return TRUE;
    }

    bool EciApi130::StartListening() {
        std::cout << "Start listening" << std::endl;
        if (SetupPort()) {
            // std::cout << "Starting context_io thread..." << std::endl ;
            io_thread_ = std::thread([this]() { io_context_.run(); });
            return true;
        }
        std::cerr
            << "[ERROR] Failed to setup port, please check if specified port exits "
            "or if you have proper permissions to access it"
            << std::endl;

        return false;
    };

    bool EciApi130::ReadSome(struct can_frame *rec_frame)
    {
        ECI_CTRL_MESSAGE stcCtrlMsg   = {0};
        DWORD dwCount = 1;

        //*** Start Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStart(dwCtrlHandle);
            //ECIDEMO_CHECKERROR(ECI130_CtrlStart);
        }

        //*** Receive Message
        hResult = ECI130_CtrlReceive( dwCtrlHandle, &dwCount, &stcCtrlMsg, ECI_RX_TIMEOUT);
        
        if(ECI_OK == hResult)
        {
            //EciPrintCtrlMessage(&stcCtrlMsg);
            BYTE bRealLen = ECI_CAN_FD_DLC_LUT[stcCtrlMsg.u.sCanMessage.u.V0.uMsgInfo.Bits.dlc];
            for(BYTE bIndex=0; bIndex < stcCtrlMsg.u.sCanMessage.u.V0.uMsgInfo.Bits.dlc; bIndex++)
                  rec_frame->data[bIndex] = stcCtrlMsg.u.sCanMessage.u.V0.abData[bIndex];

            rec_frame->can_id  = stcCtrlMsg.u.sCanMessage.u.V0.dwMsgId;
            rec_frame->can_dlc = bRealLen;
        }//endif
        
        OS_Fflush(stdout);

        //*** Stop Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStop(dwCtrlHandle, ECI_STOP_FLAG_NONE);
            // ECIDEMO_CHECKERROR(ECI130_CtrlStop);
        }
        
        //*** Reset error code
        hResult = ECI_OK;
    }//endif

    void EciApi130::ReadFromPort(struct can_frame &rec_frame) {
        auto sthis = shared_from_this();
        ReadSome(&rec_frame);

        if (sthis->rcv_cb_ != nullptr)
        {
            sthis->rcv_cb_(&sthis->rcv_frame_);
        }
        else
            sthis->DefaultReceiveCallback(&sthis->rcv_frame_);
           
        sthis->ReadFromPort(std::ref(sthis->rcv_frame_));
    }

    void EciApi130::DefaultReceiveCallback(can_frame *rx_frame) {
        std::cout << std::hex << rx_frame->can_id << "  ";
        for (int i = 0; i < rx_frame->can_dlc; i++)
            std::cout << std::hex << int(rx_frame->data[i]) << " ";
        std::cout << std::dec << std::endl;
    }

    bool EciApi130::Send2Eci(const struct can_frame &frame)
    {
        ECI_CTRL_MESSAGE stcCtrlMsg   = {0};

        //*** Start Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStart(dwCtrlHandle);
            // ECIDEMO_CHECKERROR(ECI130_CtrlStart);
        }

        if(ECI_OK == hResult)
        {
            ECI_CTRL_MESSAGE stcCtrlMsg     = {0};

            //*** Prepare CAN Message to send
            stcCtrlMsg.wCtrlClass           = ECI_CTRL_CAN;
            stcCtrlMsg.u.sCanMessage.dwVer  = ECI_STRUCT_VERSION_V0;

            stcCtrlMsg.u.sCanMessage.u.V0.dwMsgId = frame.can_id;
            // stcCtrlMsg.u.sCanMessage.u.V0.dwTime  = OS_GetTimeInMs();
            stcCtrlMsg.u.sCanMessage.u.V0.uMsgInfo.Bits.dlc = frame.can_dlc;
            
            for(BYTE bIndex=0; bIndex < stcCtrlMsg.u.sCanMessage.u.V0.uMsgInfo.Bits.dlc; bIndex++)
            {    
                stcCtrlMsg.u.sCanMessage.u.V0.abData[bIndex] = frame.data[bIndex];
            }

            hResult = ECI130_CtrlSend( dwCtrlHandle, &stcCtrlMsg, ECI_TX_TIMEOUT);

            if(ECI_OK != hResult)
            {
                OS_Printf("Error while sending CAN Messages\n");
                ECIDEMO_CHECKERROR(ECI130_CtrlSend);
                hResult = ECI_OK;
            }
        }
        OS_Fflush(stdout);

        //*** Stop Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStop(dwCtrlHandle, ECI_STOP_FLAG_NONE);
            // ECIDEMO_CHECKERROR(ECI130_CtrlStop);
        }

        return TRUE;
    }

    void EciApi130::SendFrame(const struct can_frame &frame) {
        auto sthis = shared_from_this();
        send_frame_try_count_++;
        if (Send2Eci(frame)==false)
        {    
            std::cerr << "Failed to send CAN frame" << std::endl;
            sthis->send_frame_fail_count_++;
        }
        sthis->send_frame_success_count_++;
        // std::cout << "AsyncCAN::SendFrame: total=" << send_frame_try_count_ << ", success=" << send_frame_success_count_ << ", fail=" << send_frame_fail_count_ << std::endl;

        if(send_frame_fail_count_ > 10)
        {
            throw std::ios_base::failure("AsyncCAN::SendFrame: too many CAN failures");
        }
    }

    bool EciApi130::CloseConnection(){
        
        //*** Stop Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStop(dwCtrlHandle, ECI_STOP_FLAG_NONE);
            ECIDEMO_CHECKERROR(ECI130_CtrlStop);
        }
        //*** Wait some time to ensure bus idle
        sleep(0.025);

        //*** Reset Controller
        if(ECI_OK == hResult)
        {
            hResult = ECI130_CtrlStop(dwCtrlHandle, ECI_STOP_FLAG_RESET_CTRL);
            ECIDEMO_CHECKERROR(ECI130_CtrlStop);
        }
    }

// };//namespace eci_api
