//////////////////////////////////////////////////////////////////////////
// include files
#include <ECI130.h>
#include <ECI_pshpack1.h>
#include <ECI_poppack.h>

#include <thread>
#include <mutex>
#include <functional>
#include <iostream>
#include <linux/can.h>
#include <memory>
#include <string>
#include "asio.hpp"

/** ECI send timeout in [ms] @ingroup Eci */
#define ECI_TX_TIMEOUT 200

/** ECI receive timeout in [ms] @ingroup Eci */
#define ECI_RX_TIMEOUT 200

#define DEV_ADDR "192.168.3.101"
#define DEV_PORT 19229

class AsyncPort {
public:
  AsyncPort(std::string port) : port_(port) {}
protected:
  std::string port_;
  bool port_opened_ = false;

#if ASIO_VERSION < 101200L
  asio::io_service io_context_;
#else
  asio::io_context io_context_;
#endif

  std::thread io_thread_;  
};

// namespace eci_api {
class EciApi130 :   public AsyncPort,
                    public std::enable_shared_from_this<EciApi130>
{
    public:
        using ReceiveCallback = std::function<void(can_frame *rx_frame)>;

    private:
        int can_fd_;
        #if ASIO_VERSION < 101200L
          asio::posix::basic_stream_descriptor<> socketcan_stream_;
        #else
          asio::posix::stream_descriptor socketcan_stream_;
        #endif
        
        struct can_frame rcv_frame_;

        bool SetupPort();
        void ReadFromPort(struct can_frame &rec_frame);
        void DefaultReceiveCallback(can_frame *rx_frame);
        bool Send2Eci(const struct can_frame &frame);
        ReceiveCallback rcv_cb_ = nullptr;
    public:
        EciApi130(std::string can_port = "eci130");
        void InitEciDevice();
        void SendFrame(const struct can_frame &frame);
        bool ReadSome(struct can_frame *rec_frame);
        bool StartListening();
        void SetReceiveCallback(ReceiveCallback cb) { rcv_cb_ = cb; }
        bool CloseConnection();
        
        DWORD  dwCtrlHandle  = ECI_INVALID_HANDLE;

        DWORD  hResult       = (0);
        DWORD  dwIndex       = 0;
        DWORD  dwHwIndex     = 0;
        DWORD  dwCtrlIndex   = 0;

        DWORD  dwCtrlFeatures  = 0;
        
        ECI_CTRL_CONFIG stcCtrlConfig     = {0};
        ECI_CTRL_CAPABILITIES stcCtrlCaps = {0};
        
        // ECI_CTRL_CONFIG stcCtrlConfig = {0};
        
        int send_frame_try_count_ = 0;
        int send_frame_success_count_ = 0;
        int send_frame_fail_count_ = 0;
};
// }; //namespace eci_api