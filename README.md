# Ixxat_eci_ros

[README.md на русском](https://gitlab.com/sdbcs-nio3/common/ros_drivers/cannet_nt200/-/blob/main/README.ru.md)

This lib provides ROS interface to IXXAT devices(based on Ixxat Ethernet2Can 200). 

## Adding this library to project
Threads in this lib depends on Boost, this is default library for linux(ubuntu), but check it out if error occur.

Good way to place library in project in the folder "include".

Update your CMakelists.txt to include this library with these lines:

```
add_subdirectory(include/ixxat_eci_ros)
add_library(${PROJECT_NAME}
    include/ixxat_eci_ros/src/EciApi130.cpp
    include/ixxat_eci_ros/include/EciCommon.c
)

include_directories(${PROJECT_NAME} 
include/ixxat_eci_ros
include/ixxat_eci_ros/include 
include/ixxat_eci_ros/include/core
src/eci_can)

target_link_libraries(${PROJECT_NAME} PUBLIC 
    Threads::Threads     
    core_lib
    ${USB_116}
    ${BOARD_DRIVER} 
)
```

## Usage

To start with this library needs to create pointer. There is an example to start working with ptr.
```
int main()
{
    can_name="eci130";
    ConnectEciPort(can_name, std::bind(&AgilexBase<ParserType>::ParseCANFrame,
                                            this, std::placeholders::_1));
}

void ConnectEciPort(std::string dev_name, EciApi130::ReceiveCallback cb)
{
    eci_can_ = std::make_shared<EciApi130>(dev_name);
    eci_can_->SetReceiveCallback(cb);
    eci_can_->StartListening();
}
```

Function ```StartListening``` will start a thread to reading from ECI.
To send anything to Ixxat use ```SendFrame``` to tracking count of unsuccessfull transmitions.

## Debug 

To print more debug info uncomment  ```ECIDEMO_CHECKERROR``` functions where you need.

## Test and Deploy

This library use the built-in continuous integration in GitLab. Current pipeline only checking build process in the docker.
