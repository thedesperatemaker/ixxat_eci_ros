# Ixxat_eci_ros

Данная библиотека реализует интерфейс IXXAT Ethernet2can устройств(разработано и протестировано с Ixxat Ethernet2Can 200). 

## Подключение библиотеки к проекту
Механизм потоков в этой библиотеке зависит от библиотеки Boost. Она установлена по умолчанию linux(ubuntu), однако это требует внимания в случае возникновения ошибок.

Рекомендуется размещать библиотеку в папке "include".

Отредактируйте свой CMakelists.txt для подключения библиотеки дополнив его следующими строками:

```
add_subdirectory(include/ixxat_eci_ros)
add_library(${PROJECT_NAME}
    include/ixxat_eci_ros/src/EciApi130.cpp
    include/ixxat_eci_ros/include/EciCommon.c
)

include_directories(${PROJECT_NAME} 
include/ixxat_eci_ros
include/ixxat_eci_ros/include 
include/ixxat_eci_ros/include/core
src/eci_can)

target_link_libraries(${PROJECT_NAME} PUBLIC 
    Threads::Threads     
    # ${board_lib}
    core_lib
    ${USB_116}
    ${BOARD_DRIVER} 
)
```

## Использование

Для начала работы библиотеки необходимо создать указатель. Ниже представлен пример начала работы с библиотекой.
```
int main()
{
    can_name="eci130";
    ConnectEciPort(can_name, std::bind(&AgilexBase<ParserType>::ParseCANFrame,
                                            this, std::placeholders::_1));
}

void ConnectEciPort(std::string dev_name, EciApi130::ReceiveCallback cb)
{
    eci_can_ = std::make_shared<EciApi130>(dev_name);
    eci_can_->SetReceiveCallback(cb);
    eci_can_->StartListening();
}
```

Функция ```StartListening``` стартует поток чтения из драйвера ECI.
Чтобы начать отправку в драйвер Ixxat используется функция  ```SendFrame``` также мониторящая количество неудачных отправок..

## Отладка

Для отобрадения дополнительной отладочной информации из драйвера Ixxat необходимо раскомментировать  ```ECIDEMO_CHECKERROR``` в функциях, где это необходимо.

## Тест и развертывание

Эта библиотека используется встроенный в GitLab механизм CI/CD. Сейчас проверяется только процесс сборки библиотеки в docker.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## License
For open source projects, say how it is licensed.
